#!/bin/bash -
#===============================================================================
#
#          FILE: plugInstall.sh
#
#         USAGE: ./plugInstall.sh
#
#   DESCRIPTION:
#
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (),
#  ORGANIZATION:
#       CREATED: 07/04/22 08:49
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

echo -e "Downloading VimPlug under ~/.vim/autoload"
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

